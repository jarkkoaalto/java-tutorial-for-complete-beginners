package Section02;

class Person2{
	String name;
	int age;
	
	void speak() {
		System.out.println("My name is: " + name);
	}
	
	int calculateYearsToRetirement(){
		int yearsLeft = 65 - age;
		
		// System.out.println(yearsLeft);
		return yearsLeft;
	}
	
	int getAge(){
		return age;
	}
	String getName() {
		return name;
	}
}

public class GettersAndReturnValue {
	public static void main(String[]args) {
		Person2 person2 = new Person2();
		
		person2.name = "Joe";
		person2.age = 46;
		
		// person2.speak();

		int years = person2.calculateYearsToRetirement();
		System.out.println("Years till retirements " + years);
		
		int age = person2.getAge();
		String name = person2.getName();
		System.out.println("Name is " + name);
		System.out.println("Age is " + age);
	}

}
