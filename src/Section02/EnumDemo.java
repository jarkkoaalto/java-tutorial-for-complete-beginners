package Section02;

public class EnumDemo {
	
	public static void main(String[] args) {

		Animal animal = Animal.CAT;
		
		switch(animal) {
		case DOG:
			System.out.println("DOG");
			break;
		case CAT:
			System.out.println("CAT");
			break;
		case MOUSE:
			System.out.println("MOUSE");
			break;
		default:
			break;
		}
		System.out.println(Animal.DOG);
		System.out.println(Animal.DOG.getClass());
		System.out.println(Animal.DOG instanceof Enum);
		
		
		System.out.println(Animal.MOUSE.getName());

	}

}
