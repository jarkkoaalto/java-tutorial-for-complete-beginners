package Section02;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.text.ParseException;

public class ExceptionDemo {

	public static void main(String[] args) {

		MultibleExceptionDemo test = new MultibleExceptionDemo();
		/*
		try {
			test.run();
		}catch(IOException e) {
			e.printStackTrace();
		}catch(ParseException e) {
			System.out.println("Couldn�t parse command file");
		}
		*/
		
		/*
		try {
			test.run();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		try {
			test.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			test.input();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
