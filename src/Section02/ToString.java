package Section02;

class Froggs{
	
	private int id;
	private String name;
	
	public Froggs(int id, String name){
		this.id = id;
		this.name = name;
		
	}
	public String toString() {
		
		return String.format("%-4d: %s", id, name);
		/*
		StringBuilder sb = new StringBuilder();
		sb.append(id).append(": ").append(name);

		return sb.toString();
		*/
	}
	
}

public class ToString {
	public static void main(String [] args) {
		Froggs froggs1 = new Froggs(5, "Saku");
		System.out.println(froggs1);
		
	}

}
