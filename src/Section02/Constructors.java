package Section02;

class Machine {
	
	private String name;
	private int code;
	
	public Machine() {
		System.out.println("Constructor running");
		
	}
	
	public Machine(String name) {
		System.out.println("Second Constructor Running");
		this.name = name;
	}
	
	public Machine(String name, int code) {
		System.out.println("Third Constructor Running");
		this.name = name;
		this.code = code;
	}
}

public class Constructors {
	
	public static void main(String [] args) {
		Machine machine1 = new Machine();
	
		Machine machine2 = new Machine("Arnie");
		
		Machine machine3 = new Machine("Bernie", 54);
		
	}

}
