package Section02;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) throws FileNotFoundException {
	
		String fileName = "C:\\Users\\Jarkko\\Desktop\\test.txt";

		File textFile = new File(fileName);
		
		Scanner in = new Scanner(textFile);
	
		int value = in.nextInt();
		System.out.println("Read value: " + value);
		
		while(in.hasNextLine()) {
			String line = in.nextLine();
			System.out.println(line);
		}
		
		in.close();
	}

}
