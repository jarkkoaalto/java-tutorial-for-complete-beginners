/**
 * 
 */
package Section02;

import world.Plant;

/**
 * @author Jarkko
 *
 */
public class PublicPrivateProtected {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	Plant plant = new Plant();
	System.out.println(plant.name);
	System.out.println(plant.ID);
	
	// Won't work --- type is private
	// System.out.println(plant.type);
	
	// size is protected; Classs is not the same packege as Plant.
	// won't work
	// System.out.println(plant.size);
	
	// no access specifier; Class is not in the same packege as Plant.
	// won't work
	// System.out.println(plant.size);
			
	// Won't work, Class and Plant in different packages, height package-level visibility
	// System.out.println(this.height);

	}

}
