package Section02;

class Thing{
	
	public final static int LUCKY_NUMBER = 32;
	
	public String name;
	public static String description;
	
	public int id;
	
	public static int count = 0;
	
	public Thing(){
		
	id = count;
	count++;	
	}
	
	public void showName() {
		System.out.println("Object id: " + id + " : " + description + ": " + name);
	}
	
	public static void showInfo(){
		System.out.println(description);
		// Won't work -> System.out.println(name);
	}
}

public class StaticAndFinal {

	public static void main(String [] args){
		
		Thing.description = "I am a thing";
		
		System.out.println(Thing.description);
		
		System.out.println("Before creating objects: " + Thing.count);
		
		Thing thing1 = new Thing();
		Thing thing2 = new Thing();
		
		System.out.println("After creating objects: " + Thing.count);
		
		Thing.showInfo();
		
		thing1.name = "Bob";
		thing2.name = "Sue";
		
		System.out.println(thing1.name);
		System.out.println(thing2.name);
		
		thing1.showName();
		thing2.showName();
		
		System.out.println(Math.PI);
		
		System.out.println(Thing.LUCKY_NUMBER);	
	}
}
