package Section02;

class Person111{
	// Instance variables(data or "state")
	String name;
	int age;
	
	// Classes can contain
	//1. Data
	//2. Subroutines(methods)
	
}

public class Classes_and_Objects {
	// Sub routine
	public static void main(String [] args) {
		Person111 person1 = new Person111();
		person1.name ="Joe Vloggs";
		person1.age = 45;
		
		Person111 person2 = new Person111();
		person2.name = "Vera Starr";
		person2.age = 42;
		
		Person111 person3 = new Person111();
		person3.name = "Vall Mart";
		person3.age = 32;
		
		System.out.println(person1.name + " " + person1.age);
		System.out.println(person2.name + " " + person2.age);
		System.out.println(person3.name + " " + person3.age);
	}

}
