package Section02;

import java.util.Scanner;

public class DoWhile {

	public static void main(String [] args) {
		
		/*
		// #########################################
		// Toimiva mutta huono esimerkki
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter aa number :");
		
		int value = scanner.nextInt();
		
		while(value != 5) {
			System.out.println("Enter a number");
			value = scanner.nextInt();
		}
		System.out.println("Got 5!");
		// #########################################
		*/
		Scanner scan = new Scanner(System.in);
		int value = 0;
		
		
		do {
			System.out.println("Enter a number: ");
			value = scan.nextInt();
		}
		while(value != 5);
		
		System.out.println("Got 5 !");
	}
}
