package Section02;

public class Interfaces {
public static void main(String []args){
	Machine33 mac = new Machine33();
	mac.start();
	
	MachinePerson  person1 = new MachinePerson("Bob");
	person1.greet();
	
	Info info1 = new Machine33();
	info1.showInfo();
	
	Info info2 = person1;
	info2.showInfo();
	
	System.out.println("+----------+");
	
	outputInfo(mac);
	outputInfo(person1);
}

	private static void outputInfo(Info info) {
		info.showInfo();
	}
}
