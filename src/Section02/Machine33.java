package Section02;

public class Machine33 implements Info {
	private int id = 7;
	
	public void start(){
		System.out.println("Machine33 started");
	}

	@Override
	public void showInfo() {
		System.out.println("Machine33 id is: " + id);
		
	}

}
