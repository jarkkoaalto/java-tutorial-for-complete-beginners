package Section02;

public class Ifstatement {
 public static void main(String [] args) {
	 
	 boolean cond = 4 > 8;
	 boolean cond1 = 3 == 4;
	 
	 int myInt = 20;
	 if(myInt < 10){
		 System.out.println("Yes it is less than that");
	 }
	 else if(myInt == 20 ){
		 System.out.println("That is the number");
	 }
	 else{
		 System.out.println("Typed number is large");
	 }
	 
	 System.out.println(cond);
	 System.out.println(cond1);
	 
	 
	 int loop = 0;
	 while(loop < 5){
		 System.out.println("Looping: " + loop);
		 loop = loop + 1;
	 }
	 
	 int looping  = 2;
	 while(true){
		 if(looping == 5){
			 break;
		 }
		 looping = looping + 1;
		 System.out.println("Looping again:" + looping);
	 }
	 
	 System.out.println("looping: " + looping);
			 
 }
}
