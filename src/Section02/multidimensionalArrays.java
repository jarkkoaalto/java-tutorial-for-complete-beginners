package Section02;

public class multidimensionalArrays {
	public static void main(String[]args) {
	// one dimension Array
		int [] values = {2,5,3456};
		System.out.println(values[2]);
		
	// multivalue dimension arrays
		int[][] grid = {
				{2,5,3456,2},
				{4,5,35,44},
				{1,2,3,4}
		};
		
		System.out.println(grid[1][2]);
		System.out.println(grid[2][1]);
		System.out.println(grid[0][2]);
		
		
		String[][] texts = new String[2][3];
		texts[0][0] = "Hello there";
		System.out.println(texts[0][0]);
		
		for(int row = 0; row < grid.length; row++) {
			for(int col=0; col<grid[row].length; col++) {
				System.out.print(grid[row][col] + "\t");
			}
			System.out.println(" ");
		}
	}
}
