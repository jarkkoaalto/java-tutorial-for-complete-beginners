package Section02;

public class Polymorphism {

	public static void main(String[] args) {
		Plant plant1 = new Plant();
		Tree tree = new Tree();

		Plant plant2 = tree;
		plant2.grow();
		
		tree.shedLeaves();
		
		//plant2.shedLeaves()
		dogrow(tree);
	}
	
	public static void dogrow(Plant plant){
		plant.grow();
	}

}
