package Section02;

class Koneikko{
	public void start() {
		System.out.println("Starting machine ....");
	}
}


interface Planttus{
	public void grow();
}

public class AnonymousClasses {

	public static void main(String[] args) {
	
		Koneikko koneikko = new Koneikko() {
			@Override
			public void start() {
				System.out.println("Kamera snapping ...");
			}
		};
		koneikko.start();
		
		Planttus plant1 = new Planttus() {
			@Override
			public void grow() {
				System.out.println("Plant starting Growing ...");
			}
		};
		
		plant1.grow();
	}

}
