package Section02;

public class MachinePerson implements Info {
	private String name;
	
	public MachinePerson(String name) {
		this.name = name;
	}

	public void greet(){
		System.out.println("Hello There");
	}

	@Override
	public void showInfo() {
		System.out.println("Person name is: " + name);
		
	}

}
