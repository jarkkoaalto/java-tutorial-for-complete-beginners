package Section02;

public class RobotDemo {
	
	private int id;
	
	private class Brain{
		public void think() {
			System.out.println("Robot " + id + " is thinking.");
		}
	}
	
	public static class Battery{
		public void charge() {
			System.out.println("Battery charging ... ");
		}
	}

	public RobotDemo(int id) {
		this.id = id;
	}
	
	public void start() {
		System.out.println("Starting Robor " + id);
		
		Brain brain = new Brain();
		brain.think();
	}
	final String name = "Robert";
	
	class Temp{
		public void doShomething() {
			System.out.println("ID is: " + id);
			System.out.println("My name: " + name);
		}
		
		
		
	}
	
		
}
