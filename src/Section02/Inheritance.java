package Section02;

public class Inheritance {
	
	public static void main(String [] args) {
		Machinery machine1 = new Machinery();
		machine1.start();
		machine1.stop();
		
		Car car1 = new Car();
		car1.start();
		car1.wipeWindShield();
		car1.stop();
	}

}
