package Section02;

public class CastingNumericalValues {

	public static void main(String[] args) {
		
		byte byteValue = 30;
		short shortValue = 55;
		int intValue = 888;
		long longValue = 2223333L;
		float floatValue = 8834.5F;
		double doubleValue = 43.34;
		
		System.out.println(Byte.MAX_VALUE);
		System.out.println(intValue);
		
		intValue = (int)longValue;
		System.out.println(intValue);
		
		doubleValue = intValue;
		System.out.println(doubleValue);
		
		intValue = (int)longValue;
		System.out.println(intValue);
		
		byteValue = (byte)128;
		System.out.println(byteValue);

	}

}
