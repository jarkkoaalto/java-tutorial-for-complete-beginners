package Section02;

import java.util.Collection;

public class Arrays {
	public static void main(String [] args){
		
		int [] values;
		values = new int[100];
		
		System.out.println(values[0]);
		values[0] = 100;
		values[1] = 1;
		values[2] = 40;
		
		System.out.println(values[0]);
		System.out.println(values[1]);
		System.out.println(values[2]);
		
		for(int i =0; i<10;i++){
			System.out.println(values[i]);
		}
		
		int[] numbers = {31,23,5,6,1,4,6,3,6,7,3,7,3,54,5};
		for(int i=0;i<numbers.length; i++){
			System.out.println(numbers[i]);
		}
	}

	public static Collection<? extends Person> asList(Person[] people) {
		// TODO Auto-generated method stub
		return null;
	}
 
}
