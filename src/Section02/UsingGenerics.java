package Section02;

import java.util.ArrayList;
import java.util.HashMap;

public class UsingGenerics {

	public static void main(String[] args) {
		
		///// Before Java 5 ////////////////////
		ArrayList list = new ArrayList();
		list.add("Apple");
		list.add("Orange");
		list.add("Banana");
		
		String fruit =(String) list.get(1);
		System.out.println(fruit);
		
		//////// Modern style ////////////////
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Cat");
		strings.add("Dog");
		strings.add("Alligator");
		
		String animal = strings.get(1);
		System.out.println(animal);
		
		
		////// There can be more than one type argument //////////////
		
		HashMap<Integer,  String> map = new HashMap<Integer, String>();
		
		
		////////// Java 7 style /////////////////////
		
		ArrayList<Integer> someList = new ArrayList<>();
	}

}
