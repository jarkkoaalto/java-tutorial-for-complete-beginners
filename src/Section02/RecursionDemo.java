package Section02;

public class RecursionDemo {

	public static void main(String[] args) {

		// E.g 4! = 4*3*2*1 (factorial 4)
		System.out.println(facetorial(3));
		
	}

	private static int facetorial(int value) {
	 //System.out.println(value);
	
	 // Method calling itself
	 if(value == 1) {
		 return 1;
	 }else {
	 return facetorial(value -1) * value;
	 }
 }
}
