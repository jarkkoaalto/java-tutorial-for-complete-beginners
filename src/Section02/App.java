package Section02;

import java.util.Scanner;

public class App {
	public static void main(String [] args) {
		
		// Create scanner object
		Scanner input = new Scanner(System.in);
		
		// output the prompt
		System.out.println("Enter a integer: ");
		
		// Wait for the user to enter something
		int value = input.nextInt();
		
		// Tell them what they entered.
		System.out.println("You entered: " + value);
	}

}
