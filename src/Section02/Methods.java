package Section02;

class Persons{
	// Instance variables (data or "state")
	String name;
	int age;
	
	// Classes can contains
	//1. Data
	//2. Subroutines (methods)
	
	// This is methods
	void speak() {
		for(int i =0; i<3; i++){
		System.out.println("Hello, my name is " + name);
		}
	}
	void sayHello() {
		System.out.println("Hello There");
	}
}
public class Methods {
	public static void main (String[] args) {
		Persons person1 = new Persons();
		person1.name = "Joe Vloggs";
		person1.age = 34;
		person1.speak();
		System.out.println(person1.name + " " + person1.age );
	}

}
