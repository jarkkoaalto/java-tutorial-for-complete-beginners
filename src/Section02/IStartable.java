package Section02;

public interface IStartable {
	public void start();
	public void stop();

}
