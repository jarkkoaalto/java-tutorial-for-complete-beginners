package Section02;

public class StringBuilderAndStringFormatting {

	public static void main(String[] args) {
		
		//Inefficient way
		String info = "";
		
		info += "My name is jarkko.";
		info += " ";
		info += "I'm a builder";
		
		System.out.println(info);

		// More effiencent way bending a text
		StringBuilder sb = new StringBuilder("");
		sb.append("My name is Sue,");
		sb.append(" ");
		sb.append("I am a lion tamer.");
		
		System.out.println(sb.toString());
		
		StringBuilder s = new StringBuilder();
		s.append("My name is Roger.")
		.append(" ")
		.append("I an a skydiver.");
		
		System.out.println(s.toString());
		
		////// Formating //////////////////////////////
		
		System.out.print("Here is some text.\tThat was a tab.\nThat was a newline");
		System.out.println("More text.");
		
		
		System.out.printf("Total cost %-5d; quantity is %d\n", 5, 120);
		
		for(int i=0; i<20;i++) {
			System.out.printf("%-2d: some text here\n", i);
		}
		
		for(int i=0; i<20;i++) {
			System.out.printf("%-2d: %s\n", i, "Pitkaperjantai");
		}
		
		// Formatting floating point value
		System.out.printf("Total value: %.2f\n", 4.5);
		System.out.printf("Total value: %.1f\n", 343.454033);
		
		
		
	}

}
