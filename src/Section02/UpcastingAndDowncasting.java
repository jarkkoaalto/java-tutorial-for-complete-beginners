package Section02;



class Machine6{
	public void start(){
		System.out.println("Machine Started");
	}
}

class Camera extends Machine6{
	public void start(){
		System.out.println("Camera started");
	}
	public void snap(){
		System.out.println("Photo taken");
	}
}

public class UpcastingAndDowncasting {
	public static void main(String[]args){
		Machine6 mac = new Machine6();
		Camera cam = new Camera();
		
		mac.start();
		cam.start();
		cam.snap();
		
		
		// UPCASTING
		Machine6 mac2 = mac;
		mac2.start();
		// mac2.snap(); error
		
		// DOWNCASTING
		Machine6 mac3 = new Camera();
		Camera cam2 = (Camera)mac3;
		cam2.snap();
		
		// Doesn't work --- runtime error
		Machine6 mac4 = new Machine6();
		//Camera cam3 = (Camera)mac4;
		//cam3.start();
		//cam3.snap();
	}
}
