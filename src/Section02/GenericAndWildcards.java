package Section02;

import java.util.ArrayList;

class Kone{

	@Override
	public String toString() {
		return "I am a Kone";
	}
	
	public void start() {
		System.out.println("Kone k�ynniss�");
	}
	
}

class Kamera{
	@Override
	public String toString() {
		return "I am a Kamera";
	}
	public void snap() {
		System.out.println("Kuva otettu");
	}
	
}

public class GenericAndWildcards {

	public static void main(String [] args) {
		
		ArrayList<Kone> list1 = new ArrayList<Kone>();
		list1.add(new Kone());
		list1.add(new Kone());
		showList(list1);
		
		ArrayList<Kamera> list2 = new ArrayList<Kamera>();
		list2.add(new Kamera());
		list2.add(new Kamera());
		showList2(list2);
	}
	
	public static void showList(ArrayList<? extends Kone> list) {
		for(Kone value: list) {
			System.out.println(value);
			value.start();
		}
	}
	public static void showList2(ArrayList<? super Kamera> list) {
		for(Object value: list) {
			System.out.println(value);
		}
	}
}
