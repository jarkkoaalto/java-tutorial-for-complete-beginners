package Section02;

import java.util.Scanner;

public class Switching {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a commend: ");
		String text = input.nextLine();
		
		switch(text) {
		case "start":
			System.out.println("Machine started!");
		    break;
		case "stop":
			System.out.println("Machine stopped");
			break;
		case "running":
			System.out.println("Machine is running");
			break;
		default:
			System.out.println("Command not recognized");
		}

	}

}
