package Section02;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ReadingObjectDemo {
	public static void main(String [] args) throws ClassNotFoundException {
		System.out.println("Reading objects ...");
		
		try(FileInputStream fi = new FileInputStream("test1.txt");){
			ObjectInputStream os = new ObjectInputStream(fi);
			
			Person[] people = (Person[])os.readObject();
			
			@SuppressWarnings("unchecked")
			ArrayList<Person> peopleList = (ArrayList<Person>)os.readObject();
			
			for(Person person:people) {
				System.out.println(person);
			}
			
			
			for(Person person:peopleList) {
				System.out.println(person);
			}
			
			os.close();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

}
