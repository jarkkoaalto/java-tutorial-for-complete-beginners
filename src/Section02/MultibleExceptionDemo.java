package Section02;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.text.ParseException;

public class MultibleExceptionDemo {
	public void run() throws IOException, ParseException {
		
		// throw new IOException();
		
		throw new ParseException("Error in command list.", 2);
	}
	
	public void input() throws IOException,FileAlreadyExistsException {
		
	}
}
