package Section02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {

	public static void main(String[] args) {
		
		File file = new File("test.txt");
		BufferedReader br = null;
		
		try {
			FileReader filereader = new FileReader(file);
			
			BufferedReader bf= new BufferedReader(filereader);
			String line;
			while((line = bf.readLine()) != null) {
				System.out.println(line);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.toString());
		}catch (IOException e) {
			System.out.println("Unable to read file: " + file.toString());
		}
		finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("Unale to close file " + file);
			}catch(NullPointerException ex) {
				//  File was porbably never opened"
			}
			
		}
		
		
	}

}
