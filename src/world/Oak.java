package world;

public class Oak extends Plant{
	public Oak() {
		
		// Won't work -- type is private
		
		// This works --- size is protected, Oak is a subclass 
		this.size = "Large";
		
		
		this.height = 10;
	}

}
