package world;


/**
 * 
 * @author Jarkko
 *
 *private --- only within same class
 *public --- from anywhere
 *protected --- same calss subclass and same package 
 *no modifier --- same package only
 *
 */

public class Plant {
	// Bad practice
	public String name;
	
	// Accepatable paractice -- it's final.
	public final static int ID = 8;
	
	// private only acces this class
	private String type;
	
	protected String size;
	
	
	int height;
	
	
	public Plant(){
		this.name = "Freddy";
		this.type = "Oak";
		
		this.size = "Medium";
		this.height = 8;
	}

}
