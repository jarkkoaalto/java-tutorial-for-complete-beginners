package AbstractClassDemo;

public class Car extends Machine{

	@Override
	public void start() {
	 System.out.println("Starting Car");
		
	}

	@Override
	public void soStaff() {
		 System.out.println("Car soStaff");
		
	}

	@Override
	public void shutdown() {
		 System.out.println("Shutting down Car");
		
	}

}
