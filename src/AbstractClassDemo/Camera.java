package AbstractClassDemo;

public class Camera extends Machine{

	@Override
	public void start() {
		System.out.println("Starting Camera");
		
	}

	@Override
	public void soStaff() {
		System.out.println("SoStaff Camera");
		
	}

	@Override
	public void shutdown() {
		System.out.println("Shutdown Camera");
		
	}
	
}
