package section03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

class Perhlo implements Comparable<Perhlo>{
	private String name;
	public Perhlo(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perhlo other = (Perhlo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Perhlo perhlo) {
		int len1 = name.length();
		int len2 = perhlo.name.length();
		
		if(len1 > len2) {
			return 1;
		}
		else if(len1 < len2) {
			return -1;
		}
		else {
			return name.compareTo(perhlo.name);
		}
	}
}

public class NaturalOrderingDemo {
	
	public static void main(String [] args) {
		List<Perhlo> list = new ArrayList<Perhlo>();
		SortedSet<Perhlo> set = new TreeSet<Perhlo>();
	
		addElements(list);
		addElements(set);
		
		Collections.sort(list); // Sortaa my�s Listin
		showElements(list);
		System.out.println();
		showElements(set);
	}

	private static void addElements(Collection<Perhlo> col) {
		col.add(new Perhlo("Joe"));
		col.add(new Perhlo("Sue"));
		col.add(new Perhlo("Mike"));
		col.add(new Perhlo("Clare"));
		col.add(new Perhlo("John"));
		
	}
	
	private static void showElements(Collection<Perhlo> col) {
		for(Perhlo element : col) {
			System.out.println(element);
		}
	}
}
