package section03;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class SortedMapDemo {

	public static void main(String[] args) {
		Map<Integer, String> hashMap = new HashMap<Integer,String>();
		Map<Integer,String> linkedHashMap = new LinkedHashMap<Integer, String>();
		Map<Integer,String> treeMap = new TreeMap<Integer, String>();
	
		testMap(hashMap);
		System.out.println();
		testMap(linkedHashMap);
		System.out.println();
		testMap(treeMap);
		}

	public static void testMap(Map<Integer, String> map) {
		map.put(9,"fox");
		map.put(4,"cat");
		map.put(8,"dog");
		map.put(0,"snake");
		map.put(1,"swan");
		map.put(15,"bear");
		
		for(Integer key: map.keySet()) {
			String value = map.get(key);
			
			System.out.println(key + ": " + value);
		}
	}
}
