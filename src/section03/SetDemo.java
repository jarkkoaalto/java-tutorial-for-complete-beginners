package section03;

import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class SetDemo {
	
	public static void main(String [] args) {
		
		//Set<String> set1 = new HashSet<String>();
		
		//LinkedHasSet remembers the order you added items in
		Set<String> set1 = new LinkedHashSet<String>();
		
		if(set1.isEmpty()) {
			System.out.println("Set is empty when start");
		}
		
		set1.add("dog");
		set1.add("snake");
		set1.add("cat");
		set1.add("turtle");
		set1.add("toad");
		
		// Adding duplicate items does nothing.
		set1.add("mouse");
		
		if(set1.isEmpty()) {
			System.out.println("Set is empty");
		}
		
		System.out.println(set1);
		
		//// iteration ////
		for(String element : set1) {
			System.out.println(element);
		}
		
		//// Does set containd a given items? //
		if(set1.contains("aadvark")) {
			System.out.println("Containg aadvark");
		}
	
		if(set1.contains("cat")) {
			System.out.println("Containg cat");
		}
		
		
		///////////////////////////////////////
		
		//LinkedHasSet remembers the order you added items in
				Set<String> set2 = new LinkedHashSet<String>();
				
				if(set2.isEmpty()) {
					System.out.println("Set is empty when start");
				}
				
				set1.add("koira");
				set1.add("k��rme");
				set1.add("kissa");
				set1.add("kilpikonna");
				set1.add("rupikonna");
				
				Set<String> intersection = new HashSet<String>(set1);
				
				
				intersection.retainAll(set2);
				System.out.println(intersection);
				
	
				Set<String> difference = new HashSet<String>(set2);
				difference.removeAll(set1);
				System.out.println(difference);
	}
}
