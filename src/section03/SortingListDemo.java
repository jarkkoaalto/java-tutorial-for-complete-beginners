package section03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Personast{
	private int id;
	private String name;
	
	public Personast(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "id "+ id  + " name " + name;
	}
}

class StringLengthComparator implements Comparator<String>{
	
	@Override
	public int compare(String s1, String s2) {
		return s1.compareTo(s2);
	}
}

class ReverseAplphabeticalComparator implements Comparator<String>{
	@Override
	public int compare(String s1, String s2) {
		return -s1.compareTo(s2);
	}
}

public class SortingListDemo {

	public static void main(String[] args) {
		
		////////////// Sorting String //////////////////////
		List<String> animals = new ArrayList<String>();
		
		animals.add("cat");
		animals.add("elephant");
		animals.add("tiger");
		animals.add("lion");
		animals.add("snake");
		
		// Collections.sort(animals, new StringLengthComparator());
		Collections.sort(animals, new ReverseAplphabeticalComparator());
		for(String animal : animals) {
			System.out.println(animal);
		}

		////////////// Sorting Numbers //////////////////////
		List<Integer> numbers = new ArrayList<Integer>();
		
		numbers.add(3);
		numbers.add(36);
		numbers.add(73);
		numbers.add(40);
		numbers.add(2);
		
		Collections.sort(numbers, new Comparator<Integer>(){
			public int compare(Integer num1, Integer num2) {
				return -num1.compareTo(num2);
			}
		});
		
		for(Integer number: numbers) {
			System.out.println(number);
		}
	
	
	////////////// Sorting String //////////////////////
	List<Personast> people = new ArrayList<Personast>();
	
	people.add(new Personast(1,"Joe"));
	people.add(new Personast(2, "Bob"));
	people.add(new Personast(3, "Clare"));
	people.add(new Personast(4, "Sue"));
	people.add(new Personast(5, "Jannika"));
	
	
	System.out.println("\n");
	Collections.sort(people, new Comparator<Personast>() {
		public int compare(Personast p1, Personast p2) {
			
			return p1.getName().compareTo(p2.getName());
		}
	});
	
	for(Personast p: people) {
		System.out.println(p);
	}
	
	}

}
