package section05;

interface Executable{
	void execute();
}

class Runner{
	public void run(Executable e) {
		System.out.println("Executing code block ...");
		e.execute();
	}
}

public class App {
	
	public static void main(String [] args) {
		
		Runner runner = new Runner();
		runner.run(new Executable() {
			public void execute() {
				System.out.println("Hello There");
			}
		});
		System.out.println("===================================");
		runner.run(() -> {
			
		System.out.println("This is code passed in a lamba expression.");	
		System.out.println("Hello there");
		});
	}

}
